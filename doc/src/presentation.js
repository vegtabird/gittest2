// Import React
import React from 'react';

// Import Spectacle Core tags
import {
  BlockQuote,
  Cite,
  Deck,
  Heading,
  ListItem,
  List,
  Quote,
  Slide,
  Text,
  Link,
  Image,
  Appear
} from 'spectacle';
import mockPic from './static/image/mock.png'
import mockRePic from './static/image/mockReturn.png'
import mockSinPic from './static/image/mockSingle.png'
import highMoPic from './static/image/highMock.png'
import mockScriptPic from './static/image/mockScript.png'
import autoTestPic from './static/image/autoTest.png'
import testSetPic from './static/image/testSet.png'
import testDemoPic from './static/image/testDemo.png'
import testPic from './static/image/testDemo2.png'
import dataPic from './static/image/dataImport.png'
// Import theme
import createTheme from 'spectacle/lib/themes/default';

// Require CSS
require('normalize.css');

const theme = createTheme(
  {
    primary: 'white',
    secondary: '#1F2022',
    tertiary: '#03A9FC',
    quaternary: '#CECECE',
    blcak:'black'
  },
  {
    primary: 'Montserrat'
  }
);

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck
        transition={['zoom', 'slide']}
        transitionDuration={500}
        theme={theme}
      >
        <Slide transition={['zoom']} bgColor="white">
          <Heading size={1}  caps lineHeight={1} textColor="black" fit>
          Yapi接口管理平台
          </Heading>
          <Link href="https://yapi.ymfe.org/documents/index.html">
            官方文档
          </Link>
        </Slide>
        <Slide transition={['fade']} bgColor="white" >
          <Heading size={6} textColor="black" caps>
            功能与特点
          </Heading>
          <List textColor="black" type="1">
            <ListItem textSize={30} margin="20px">扁平化权限管理</ListItem>
            <ListItem textSize={30} margin="20px">MockServer数据模拟<Text textSize={20} >基于mockJs实现数据模拟，可视化操作以及高级期望设置</Text></ListItem>
            <ListItem textSize={30} margin="20px">自动化测试<Text textSize={20}>可视化配置，一键测试</Text></ListItem>
            <ListItem textSize={30} margin="20px">数据导入<Text textSize={20}>支持swagger,postman和har的数据导入</Text></ListItem>
          </List>
        </Slide>
        <Slide transition={['fade']} bgColor="white" textColor="black" align="flex-start center">
        <Heading size={6} textColor="black" caps>
          权限管理
        </Heading>
        <Text textSize={25}>在Yapi中引入扁平化的权限管理。超级管理员拥有最高权限，可以管理所有分组和项目，并将权限分给分组组长。组长对组内项目负责即可</Text>
        <List textColor="white" >
          <ListItem textSize={25} bgColor="blue" padding="12px">
          超级管理员
          <List  bulletStyle="-" padding="0px" margin="10px 0 0 0">
            <ListItem textSize={15}>创建分组</ListItem>
            <ListItem textSize={15}>分配组长</ListItem>
            <ListItem textSize={15}>管理所有成员信息</ListItem>
          </List>
          </ListItem>
          <ListItem textSize={25} bgColor="#5F79FE" padding="12px" margin="10px 0 0 0">
          组长
          <List  bulletStyle="-" padding="0px" >
            <ListItem textSize={15}>创建项目</ListItem>
            <ListItem textSize={15}>管理分组信息</ListItem>
            <ListItem textSize={15}>管理组内成员</ListItem>
          </List>
          </ListItem>
          <ListItem textSize={25} bgColor="#A7B2F5" padding="12px" margin="10px 0 0 0">
          开发者
          <List  bulletStyle="-" padding="0px">
            <ListItem textSize={15}>创建接口并修改</ListItem>
            <ListItem textSize={15}>不允许创建分组</ListItem>
            <ListItem textSize={15}>不允许修改分组以及项目信息</ListItem>
          </List>
          </ListItem>
        </List>
        </Slide>
        <Slide transition={['fade']} bgColor="white" textColor="black">
        <Heading size={5}   >
          MockServer
        </Heading>
        <Text textSize={25} textAlign="left">YAPI数据mock模块是基于mockJs实现，同时支持可视化的请求参数和返回数据的编辑，无需编写额外代码即可设置参数。</Text>
        <Heading size ={6} textAlign="left" margin="30px 0 0 0">MockJs</Heading>
          <Text textSize={25} textAlign="left">MockJs 是一款模拟数据生成器，主要功能是根据数据模板生成模拟数据，从而帮助前端工程师独立开发。其主要特点如下：</Text>
        <List type="1">
          <ListItem textSize={20}>帮助前后端分离</ListItem>
          <ListItem textSize={20}>可随机生成数据，模拟各种场景</ListItem>
          <ListItem textSize={20}>数据类型丰富，支持文本，数字，布尔值，日期，数组，对象，链接，图片等</ListItem>
        </List>
        </Slide>
        <Slide transition={['fade']} bgColor="white" textColor="black">
        <Heading size={5}>
          MockJs
        </Heading>
        <Image src={mockPic} marginTop={50} ></Image>
        <List  type="1">
        Mock语法规范 详情见文档:<Link href="http://mockjs.com/0.1/#%E8%AF%AD%E6%B3%95%E8%A7%84%E8%8C%83">mock</Link>
          <ListItem textSize={20}>数据模板定义规范</ListItem>
          <ListItem textSize={20}>数据占位符定义规范</ListItem>
        </List>
        </Slide>
        <Slide transition={['fade']} bgColor="white" textColor="black" align="center center">
          <Heading size={6}>
            Mock-可视化
          </Heading>
          <Text textSize={25} textAlign="left">自己编写mockJs语法有点繁琐，yapi还提供可视化的编辑操作，只需点击选项即可配置参数。如下是设置返回数据模板</Text>
          <Image src={mockRePic}></Image>
        </Slide>
        <Slide transition={['fade']} bgColor="white" textColor="black" align="center center">
          <Heading size={6}>
            Mock-可视化
          </Heading>
          <Text textSize={25} textAlign="left">每个参数都可以点击右侧的设置图标，设置具体的格式。例如数组的长度，字符串的长度，默认值等。如下是设置字符串格式</Text>
          <Image src={mockSinPic} height="500px"></Image>
        </Slide>
        <Slide transition={['fade']} bgColor="white" textColor="black">
          <Heading size={6} >
            高级mock
          </Heading>
          <Text textSize={25} textAlign="left"><b>Mock</b>除了能够返回随机数据，还能够提供高级mock功能来配合复杂的需求。</Text>
          <Text textSize={25} textAlign="left">在接口界面的“高级Mock”中有期望和脚本两种方式</Text>
          <Image src={highMoPic}></Image>
          <List  type="1">
            <ListItem textSize={20}>期望：根据不同请求的参数(参数可在body或者query中)以及ip来返回不同的HTTP code,HTTP 头和body。</ListItem>
            <ListItem textSize={20}>脚本：自行编写js脚本，根据请求参数，cookie来返回不同的数据</ListItem>
          </List>
          <Text textSize={20} textAlign="left">详情可见文档：<Link href="https://yapi.ymfe.org/documents/adv_mock.html">高级Mock</Link></Text>
        </Slide>
        <Slide transition={['fade']} bgColor="white" textColor="black">
          <Heading size={6} >
            高级mock
          </Heading>
          <Text textSize={25} textAlign="left">使用场景：例如设置如下条件，当请求中的参数type值是"error"时，返回"errorMessage",当type的值是"empty"时，返回空数组。
          则可以编写如下脚本</Text>
          <Image src={mockScriptPic}></Image>
        </Slide>
        <Slide transition={['fade']} bgColor="white" textColor="black">
          <Heading size={6} >
            自动化测试
          </Heading>
          <Text textSize={25} textAlign="left">
              yapi平台提供了自动化测试功能，免去了传统的测试中学习框架和编写代码的成本。只需要设置入口参数和response的断言条件即可。
          </Text>
          <Text textSize={25} textAlign="left">参数配置完全后，在测试集合中点击开始测试即可对接口自动化测试。测试完成后，点击测试报告可以查看测试结果</Text>
          <Image src={autoTestPic}></Image>
        </Slide>
        <Slide transition={['fade']} bgColor="white" textColor="black">
          <Heading size={6} >
            自动化测试
          </Heading>
          <Text textSize={25} textAlign="left">
            编辑测试用例时，请求参数有两种方式设置：Mock参数和变量参数
          </Text>
          <List>
            <ListItem textSize={25}>mock参数：通过mockJs生成随机数据</ListItem>
            <ListItem textSize={25}>变量参数：将前面接口的参数或者返回数据作为后面接口的请求参数。格式为："$.{"{key}"}.{ "{params|body}"}.{"{path}"}",key为接口的key值在测试集合中查看
            <Text textSize={25} textAlign="left">使用场景：例如文章内容的接口需要传入文章ID的参数，而文章Id则通过前面
            文章列表的接口获得。则可以设置文章内容的请求id为$44.body.data.list[0].id
            </Text>
            </ListItem>
              <Image src={testSetPic} alt="测试集合"></Image>
          </List>
        </Slide>
        <Slide transition={['fade']} bgColor="white" textColor="black">
          <Heading size={6} >
            自动化测试
          </Heading>
          <Image src={testPic}></Image>
          <Text  textSize={15}>文章内容接口参数</Text>
            <Image src={testDemoPic} ></Image>
              <Text  textSize={15}>文章列表返回数据</Text>
        </Slide>
        <Slide transition={['fade']} bgColor="white" textColor="black">
          <Heading size={6} >
            数据导入
          </Heading>
          <Text  textSize={25} textAlign="left">Yapi同时支持postman,swagger还有har的数据导入，方便迁移旧项目。同时也能够一键导出json,markdown,html和swaggerjson格式的接口文档</Text>
          <Text  textSize={25} textAlign="left">数据相关操作在项目界面中的"数据管理"标签页下</Text>
          <Image src={dataPic}></Image>
        </Slide>
        <Slide transition={['fade']} bgColor="white" textColor="black">
        <Heading size={6} >
          相关文档
        </Heading>
          <Text  textSize={25} textAlign="left">GitHub地址：<Link href="https://github.com/ymfe/yapi">https://github.com/ymfe/yapi</Link></Text>
          <Text textSize={25} textAlign="left">MockJs:<Link href="http://mockjs.com/">http://mockjs.com/</Link></Text>
          <Text  textSize={25} textAlign="left">Yapi官方文档:<Link href="https://yapi.ymfe.org/documents/index.html">https://yapi.ymfe.org/documents/index.html</Link></Text>
        </Slide>
      </Deck>

    );
  }
}
